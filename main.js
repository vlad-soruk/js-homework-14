"use strict"

// Реалізувати програму, яка циклічно показує різні картинки.

let images = document.querySelectorAll('.images-wrapper img');

images.forEach( (image) => image.hidden = true );

// При запуску програми на екрані має відображатись перша картинка
// Через 3 секунди замість неї має бути показано друга картинка

let imageIndex = 0;
let seconds = 1000;

// При запуску програми картинки змінюються
let isAnimating = true;

function showImage() {
    images.forEach( (image) => image.hidden = true );
    images[imageIndex].hidden = false;
    images[imageIndex].style.margin = '0 auto';
    imageIndex++;

    if (imageIndex < images.length) {
        imageTimeout = setTimeout(showImage, seconds);
    }
    // Після того, як будуть показані всі картинки - цей цикл має розпочатися наново.
    else {
        imageIndex = 0;
        imageTimeout = setTimeout(showImage, seconds);
    }
}

let imageTimeout = setTimeout(showImage);


let buttonsWrapper = document.querySelector('.buttons-wrapper');
let resumeButton = document.createElement('button');
resumeButton.textContent = 'Resume';
resumeButton.classList.add('resume-button');
buttonsWrapper.prepend(resumeButton);

resumeButton.addEventListener('click', function () {

    // Якщо при натисненні на кнопку "Продовжити" анімація не відбувається
    // (картинки не змінюються), то лише тоді цикл може продовжитись 
    if (!isAnimating) {           
        imageTimeout = setTimeout(showImage, seconds);
        isAnimating = true;
    }
});

// Після запуску програми десь на екрані має з'явитись кнопка з написом Припинити.

// Після натискання на кнопку Припинити цикл завершується, на екрані залишається показаною 
// та картинка, яка була там при натисканні кнопки.

let stopButton = document.createElement('button');
stopButton.textContent = 'Stop';
stopButton.classList.add('stop-button');
buttonsWrapper.prepend(stopButton);

stopButton.addEventListener('click', function () {
    console.log(imageIndex);
    clearTimeout(imageTimeout);

    // При натисненні кнопки "Стоп" цикл зміни картинок завершується
    isAnimating = false;
})

// Знаходимо кнопку, що буде змінювату тему

let changeThemeButton = document.querySelector('.change-theme-button');
console.log(changeThemeButton);

// При натисканні на кнопку - змінювати колірну гаму сайту (кольори кнопок, фону тощо)

function changePageTheme () {
    document.body.style.background = 'linear-gradient(90deg, rgba(131,58,180,1) 0%, rgba(253,29,29,1) 50%, rgba(252,176,69,1) 100%)';
    window.localStorage.setItem('bodyStyleBackground', document.body.style.background);

    stopButton.style.background = 'rgb(255, 150, 31)';
    stopButton.style.color = 'black';
    stopButton.style.border = '2px inset white';

    resumeButton.style.background = 'linear-gradient(to left, cyan 0% 85%, green)';
    resumeButton.style.color = 'black';
    resumeButton.style.border = '2px outset white';

    changeThemeButton.style.background = 'radial-gradient(rgb(38, 239, 175), rgb(57, 63, 36))';
    changeThemeButton.style.color = 'rgb(38, 32, 54)'; 
    changeThemeButton.style.border = '2px groove white';

    images.forEach( (image) => {
        image.style.borderStyle = 'solid';
        image.style.borderWidth = '2px';
        image.style.borderImage = 'linear-gradient(black 10%, white 30%) 1';
    } );
}

function returnToDefaultPageTheme () {
    document.body.style.background = '';
    window.localStorage.setItem('bodyStyleBackground', '');

    stopButton.style.background = '';
    stopButton.style.color = '';
    stopButton.style.border = '';

    resumeButton.style.background = '';
    resumeButton.style.color = '';
    resumeButton.style.border = '';

    changeThemeButton.style.background = '';
    changeThemeButton.style.color = ''; 
    changeThemeButton.style.border = '';

    images.forEach( (image) => {
        image.style.borderStyle = '';
        image.style.borderWidth = '';
        image.style.borderImage = '';
    } );
}

// На початку встановлюємо, що тему ніхто не змінював
let themeWasChanged = false;
changeThemeButton.addEventListener('click', function () {
    // Якщо тему ще не було змінено викликаємо функцію зміни колірної гами сторінки та
    // записуємо, що сторінка була модифікована
    if (!themeWasChanged) {
        changePageTheme();
        themeWasChanged = true;
    }
    // Якщо сторінка вже була змінена на "нарядну" версію, то після кліку все повертається до
    // дефолтного стану, а в змінну  themeWasChanged записується false
    else {
        returnToDefaultPageTheme();
        themeWasChanged = false;
    }
});

// Дивимось, яке значення заднього фону сторінки записане в LocalStorage
let pageTheme = localStorage.getItem('bodyStyleBackground'); 

// Якщо там пуста строка, то все так і залишається після перезавантаження браузера
if (pageTheme === '') {
    returnToDefaultPageTheme();
}
// Інакше - після перезавантаження браузера змінюємо тему на "нарядну" та записуємо, що 
// тему сторінки було модифіковано. Робимо це для того, щоб після натискання на кнопку 
// "Change theme" у нас спрацьовував блок "else" івента кнопки та одразу тема сторінки 
// змінювалась на дефолтну
else {
    changePageTheme();
    themeWasChanged = true;
}